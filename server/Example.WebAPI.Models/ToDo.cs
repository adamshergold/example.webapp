﻿using System;

namespace Example.WebAPI.Models
{
    public sealed record ToDo(string Id, string Title, bool IsDone)
    {
        public ToDo(string title, bool isDone) : this(Guid.NewGuid().ToString(), title, isDone)
        {
        }
        
        public ToDo(string title) : this(Guid.NewGuid().ToString(), title, false)
        {
        }
    }
} 
