using Example.WebAPI.Models;
using Example.WebAPI.Server.Controllers;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;

namespace Example.WebAPI.Server.Services;

public class GoogleCloudToDoService : IToDoService
{
    private readonly ILogger<GoogleCloudToDoService> _logger;
    
    private readonly IHttpContextAccessor _httpContextAccessor;

    private readonly IConfiguration _configuration;

    private readonly GoogleCredential _googleCredential;
    
    private readonly StorageClient _storageClient;
    
    private readonly string _bucketName;

    private readonly string _defaultName = "0";
    
    public GoogleCloudToDoService(
        ILogger<GoogleCloudToDoService> logger,
        IHttpContextAccessor httpContextAccessor,
        IConfiguration configuration)
    {
        _logger = logger;
        
        _httpContextAccessor = httpContextAccessor;
        
        _configuration = configuration;
        
        _bucketName = _configuration["GoogleCloudToDoService:Bucket"];

        var credentialBase64 =
            _configuration["GoogleCloudToDoService:Credentials"];
        
        if (string.IsNullOrEmpty(credentialBase64))
        {
            throw new Exception("'GoogleCloudToDoService:Credentials' configuration not available");
        }

        var base64EncodedBytes = Convert.FromBase64String(credentialBase64);

        var credentialJson =
            System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        
        _googleCredential = GoogleCredential.FromJson(credentialJson);
        
        _storageClient = StorageClient.Create(_googleCredential);
    }

    private string UserId()
    {
        var name = _httpContextAccessor.HttpContext?.User.Identity?.Name;
        return string.IsNullOrEmpty(name) ? _defaultName : name;
    }

    public IEnumerable<ToDo> Items()
    {
        _logger.LogInformation("GoogleCloudToDoService:Items()");

        var prefix = UserId();

        return
            _storageClient
                .ListObjects(_bucketName, prefix)
                .Select(obj =>
                {
                    using var ms = new MemoryStream();
                    _storageClient.DownloadObject(obj, ms);
                    ms.Position = 0L;
                    return System.Text.Json.JsonSerializer.Deserialize<ToDoDto>(ms)?.ToModel();
                })
                .WhereNotNull()
                .ToArray();
    }

    public ToDo Add(string title)
    {
        _logger.LogInformation("GoogleCloudToDoService:Add({Title}) | Adding", title);
        
        var toDo = new ToDo(title);

        using var ms = new MemoryStream();
        
        System.Text.Json.JsonSerializer.Serialize(ms, ToDoDto.FromModel(toDo));

        var key = $"{UserId()}/{toDo.Id}";

        var result =_storageClient.UploadObject(_bucketName, key, "application/json", ms);

        if (result is null)
        {
            throw new Exception("UploadObject failed");
        }
        
        _logger.LogInformation("GoogleCloudToDoService:Add({Title}) | UploadObject {Object}", title, result);
        
        return toDo;
    }

    public bool Delete(string id)
    {
        var key = $"{UserId()}/{id}";
        
        _storageClient.DeleteObject(_bucketName, key);

        return true;
    }

    public bool Update(ToDo toDo)
    {
        _logger.LogInformation("GoogleCloudToDoService:Update({ToDo}) | Update", toDo);
        
        using var ms = new MemoryStream();
        
        System.Text.Json.JsonSerializer.Serialize(ms, ToDoDto.FromModel(toDo));

        var key = $"{UserId()}/{toDo.Id}";

        var result =_storageClient.UploadObject(_bucketName, key, "application/json", ms);

        if (result is null)
        {
            throw new Exception("UploadObject failed");
        }
        
        _logger.LogInformation("GoogleCloudToDoService:Update({Title}) | UploadObject {Object}", toDo, result);
        
        return true;
    }
}