using Example.WebAPI.Models;
using Example.WebAPI.Server.Controllers;

namespace Example.WebAPI.Server.Services;

public class FileToDoService : IToDoService
{
    private readonly ILogger<FileToDoService> _logger;
    
    private readonly IHttpContextAccessor _httpContextAccessor;

    private readonly IConfiguration _configuration;
    
    private readonly string _root;

    private readonly string _defaultName = "0";
    
    private string UserId()
    {
        var name = _httpContextAccessor.HttpContext?.User.Identity?.Name;
        return string.IsNullOrEmpty(name) ? _defaultName : name;
    }
    
    public FileToDoService(ILogger<FileToDoService> logger, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
    {
        _logger = logger;
        
        _httpContextAccessor = httpContextAccessor;
        
        _configuration = configuration;
        
        _root = string.IsNullOrEmpty(configuration["FileToDoService:Root"])
            ? Path.GetTempPath()
            : configuration["FileToDoService:Root"];
    }

    public IEnumerable<ToDo> Items()
    {
        var folder = Path.Combine(_root, UserId());
        Directory.CreateDirectory(folder);
        
        _logger.LogInformation("FileToDoService::Items() | Reading from {Folder}", folder);
        
        return
            Directory.EnumerateFiles(folder)
                .Select(file =>
                {
                    var json = File.ReadAllText(file);
                    return System.Text.Json.JsonSerializer.Deserialize<ToDoDto>(json);
                })
                .WhereNotNull()
                .Select(dto => dto.ToModel())
                .ToArray();
    }

    public ToDo Add(string title)
    {
        _logger.LogInformation("FileToDoService:Add({Title}) | Adding", title);

        var folder = Path.Combine(_root, UserId());
        Directory.CreateDirectory(folder);
        
        var toDo = new ToDo(title);
        
        var file = Path.Combine(folder, $"{toDo.Id}.json");

        var json = System.Text.Json.JsonSerializer.Serialize(ToDoDto.FromModel(toDo));
        
        _logger.LogInformation("FileToDoService:Add({Title}) | Writing to {File}", title, file);
        
        File.WriteAllText(file, json);
        
        return toDo;
    }

    public bool Delete(string id)
    {
        var folder = Path.Combine(_root, UserId());

        var file = Path.Combine(folder, $"{id}.json");
        
        _logger.LogInformation("FileToDoService:Delete({Id}) | Deleting {File}", id, file);

        if (!File.Exists(file))
        {
            return false;
        }
        
        File.Delete(file);

        return true;
    }

    public bool Update(ToDo item)
    {
        throw new NotImplementedException();
    }
}