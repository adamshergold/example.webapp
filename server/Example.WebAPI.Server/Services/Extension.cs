namespace Example.WebAPI.Server.Services;

public static class Extension {
    public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> o) where T:class
        => o.Where(x => x != null)!;
}