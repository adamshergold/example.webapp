using System.Collections.Generic;
using Example.WebAPI.Models;

namespace Example.WebAPI.Server.Services
{
    public interface IToDoService
    {
        public IEnumerable<ToDo> Items();

        public ToDo Add(string title);

        public bool Delete(string id);

        public bool Update(ToDo item);
    }
}