using Example.WebAPI.Models;

namespace Example.WebAPI.Server.Services;

public class InMemoryToDoService : IToDoService
{
    private readonly IList<ToDo> _items;

    private readonly IHttpContextAccessor _httpContextAccessor;
        
    public InMemoryToDoService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
            
        _items = new List<ToDo>();
    }
        
    public IEnumerable<ToDo> Items()
    {
        return _items;
    }

    public ToDo Add(string title)
    {
        var item = new ToDo(title);
        _items.Add(item);
        return item;
    }

    public bool Delete(string id)
    {
        var candidate = 
            _items.FirstOrDefault(item => string.Equals(item.Id, id, StringComparison.Ordinal));

        if (candidate is null)
        {
            return false;
        }

        _items.Remove(candidate);

        return true;
    }

    public bool Update(ToDo item)
    {
        var candidate =
            _items.FirstOrDefault(v => string.Equals(v.Id, item.Id, StringComparison.Ordinal));

        if (candidate is null)
        {
            return false;
        }

        _items.Remove(candidate);
        _items.Add(item);

        return true;
    }
}