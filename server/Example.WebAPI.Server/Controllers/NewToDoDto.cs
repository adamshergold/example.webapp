namespace Example.WebAPI.Server.Controllers
{
    public class NewToDoDto
    {
        public string Title { get; set; }

        public NewToDoDto(string title)
        {
            Title = title;
        }
    }
}