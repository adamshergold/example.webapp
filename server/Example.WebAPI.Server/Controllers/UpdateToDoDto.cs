using Example.WebAPI.Models;

namespace Example.WebAPI.Server.Controllers;

public class UpdateToDoDto
{
    public string Id { get; set; }
    public string Title { get; set; }
    public bool IsDone { get; set; }
    public UpdateToDoDto(string id, string title, bool isDone)
    {
        Id = id;
        Title = title;
        IsDone = isDone;
    }

    public ToDo ToModel()
    {
        return new ToDo(this.Id, this.Title, this.IsDone);
    }
}    
