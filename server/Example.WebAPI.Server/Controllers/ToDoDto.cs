using Example.WebAPI.Models;

namespace Example.WebAPI.Server.Controllers
{
    public class ToDoDto
    {
        public string Id { get; set; }
        public string Title { get; set; }

        public bool IsDone { get; set; }
        public ToDoDto(string id, string title, bool isDone)
        {
            Id = id;
            Title = title;
            IsDone = isDone;
        }
        
        public static ToDoDto FromModel(ToDo item)
        {
            return new ToDoDto(item.Id, item.Title, item.IsDone);
        }

        public ToDo ToModel()
        {
            return new ToDo(Id, Title, IsDone);
        }
    }
}