using System.Collections.Generic;
using System.Linq;
using Example.WebAPI.Server.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Example.WebAPI.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ToDoController : ControllerBase
    {
        private readonly ILogger<ToDoController> _logger;

        private readonly IToDoService _toDoService;

        public ToDoController(ILogger<ToDoController> logger, IToDoService toDoService)
        {
            _logger = logger;
            _toDoService = toDoService;
        }

        [HttpGet("items")]
        [Authorize]
        public IEnumerable<ToDoDto> Items()
        {
            _logger.LogInformation("ToDoController::Items() | User {User}", HttpContext.User);
            
            var toDos =
                _toDoService.Items().Select(ToDoDto.FromModel).ToArray();

            return toDos;
        }
        
        [HttpPost("items")]
        [Authorize]
        public ToDoDto Add(NewToDoDto newToDoDto)
        {
            _logger.LogInformation("ToDoController::Add({NewToDoDto})", newToDoDto);
            
            return ToDoDto.FromModel(_toDoService.Add(newToDoDto.Title));
        }

        [HttpDelete("items/{id}")]
        [Authorize]
        public ActionResult Delete(string id)
        {
            if (!_toDoService.Delete(id))
            {
                return NotFound();
            }

            return Ok();
        }

        [HttpPut("items")]
        [Authorize]
        public ActionResult Put(UpdateToDoDto updateToDoDto)
        {
            if (!_toDoService.Update(updateToDoDto.ToModel()))
            {
                return NotFound();
            }

            return Ok();
        }
        
    }
}
