import Vue from 'vue'
import VueRouter from 'vue-router'

import ToDoList from '../components/todo-list.vue'
import About from '../components/about.vue'

import { authenticationGuard } from '@/auth/authenticationGuard';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ToDoList',
    component: ToDoList,
    beforeEnter: authenticationGuard,
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },

  // {
  //   path: '/about',
  //   name: 'About',
  //   component: Aboue
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
